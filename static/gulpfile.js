'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var sourcemaps = require('gulp-sourcemaps');

gulp.task('browser-sync', function() {
    browserSync.init({
        proxy: {
        	target: 'localhost/vjapp'
        },
        notify: {
        	styles: [
                'position: fixed;',
        		'bottom:0px;',
                'right: 0px;',
        		'font-size:10px;',
        		'padding:2px;'
        	]
        }
    });
});

gulp.task('sass', function () {
  return gulp.src('./sass/app.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('../build/css'))
    .pipe(browserSync.stream());
    ;
});
 
gulp.task('default', ['sass'], function () {
  gulp.watch('./sass/**/*.scss', ['sass']);
});