<div class="ui right sidebar inverted vertical menu" id="Menu">
  <div class="item header">
    Catalogue
  </div>
  <a href='#' class="item">
    <i class="icon shopping bag"></i>All Products
  </a>

  <div class="item header">
    Contacts
  </div>
  <a href="#" class="item">
    <i class="diamond icon"></i>All Contacts 
  </a>

  <div class="item header">
    Manage
  </div>
    <div class="item subheader">
      Users
    </div>
      <a href="#" class="item">
        <i class="users icon"></i>All Users 
      </a>
      <a href="#" class="item">
        <i class="add user icon"></i> Add User
      </a>
    
    <div class="item subheader">
      Categories
    </div>
      <a href="#" class="item">
        <i class="sitemap icon"></i> List all categories
      </a>
      <a href="#" class="item">
        <i class="plus icon"></i> Add Category
      </a>

    <div class="item subheader">
      Products
    </div>
      <a href="#" class="item">
        <i class="shopping bag icon"></i> List all products
      </a>
      <a href="#" class="item">
        <i class="plus square outline icon"></i> Add Product
      </a>

    <div class="item subheader">
      Contacts
    </div>
      <a href="#" class="item">
        <i class="users icon"></i> List all Contacts
      </a>
      <a href="#" class="item">
        <i class="add user icon"></i> Add Contact
      </a>
      <a href="#" class="item">
        <i class="tags icon"></i> Manage Tags
      </a>

    <div class="item subheader">
      SMS Blast
    </div>
      <a href="#" class="item">
        <i class="comments icon"></i> Send SMS Blast
      </a>

    <div class="item subheader">
      SMS Schedule
    </div>
      <a href="#" class="item">
        <i class="calendar icon"></i> Scheduled SMS Campaigns
      </a>
      <a href="#" class="item">
        <i class="add to calendar icon"></i> Add Campaign
      </a>

    <div class="item subheader">
      Settings
    </div>
      <a href="#" class="item">
        <i class="settings icon"></i> Settings
      </a>
</div>