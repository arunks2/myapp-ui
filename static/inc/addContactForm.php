<form action="#" class="ui form">
                <div class="two fields">
                  <div class="field">
                    <label for="">First Name</label>
                    <input type="text" autofocus>
                  </div>
                  <div class="field">
                    <label for="">Last Name</label>
                    <input type="text">
                  </div>
                </div>
                <div class="two fields">
                  <div class="field">
                    <label for="">Email</label>
                    <input type="text">
                  </div>
                  <div class="field">
                    <label for="">Mobile Number</label>
                    <input type="text">
                  </div>
                </div>
                <div class="two fields">
                  <div class="field">
                    <label for="">Home Phone</label>
                    <input type="text">
                  </div>
                  <div class="field">
                    <label for="">Gender</label>
                    <select name="" id="" class="ui selection dropdown">
                      <option value="M">Male</option>
                      <option value="F">Female</option>
                      <option value="O">Others</option>
                    </select>
                  </div>
                </div>
                <hr>
                <div class="two fields">
                  <div class="field">
                    <label for="">Date of Birth</label>
                    <input type="text">
                  </div>
                  <div class="field">
                    <label for="">Date of Anniversary</label>
                    <input type="text">
                  </div>
                </div>
                <hr>
                  <div class="field">
                    <label for="">Address Line 1</label>
                    <input type="text">
                  </div>
                  <div class="field">
                    <label for="">Address Line 2</label>
                    <input type="text">
                  </div>
                <div class="two fields">
                  <div class="field">
                    <label for="">City</label>
                    <select name="" id="" class="ui selection search dropdown">
                      <option value="Delhi">Delhi</option>
                      <option value="Mumbai">Mumbai</option>
                    </select>
                  </div>
                  <div class="field">
                    <label for="">State</label>
                    <select name="" id="" class="ui selection search dropdown">
                      <option value="UP">UP</option>
                      <option value="Delhi">Delhi</option>
                    </select>
                  </div>
                </div>
                <div class="two fields">
                  <div class="field">
                    <label for="">Pin Code</label>
                    <input type="text">
                  </div>
                  <div class="field">
                    <label for="">Country</label>
                    <input type="text" value="India">
                  </div>
                </div>
                <hr>
                <div class="field">
                  <label for="">Tags</label>
                  <input type="text">
                </div>
                <div class="field">
                  <label for="">Notes</label>
                  <textarea name="" id="" rows="3"></textarea>
                </div>
                <button class="ui large button Gold">Save Contact</button>
              </form>