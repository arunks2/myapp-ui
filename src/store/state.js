module.exports = {
  loader: 1,
  loaderMessage: 'Working',
  token : localStorage.getItem('token') ? localStorage.getItem('token') : 0,
  user: localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : {
    name: '',
    email: '',
    mobile_number:'',
    is_mobile_verified:'',
    is_email_verified:''
  },
  showHeader: false,
  showSideBar: false,
  customer_array: new  Array(),
  product_ids: localStorage.getItem('product_ids') ? JSON.parse(localStorage.getItem('product_ids')) : new Array(),
  // Added global chunk property
  pagination_chunk: 0
}