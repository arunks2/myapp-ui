module.exports = {
    showLoader (state,message) {
      state.loader = 1
      if(typeof(message) !== undefined) {
        state.loaderMessage = message
      }
    },
    hideLoader (state) {
      state.loader = 0
    },
    loggedIn (state, message) {
      localStorage.setItem('token', message)
    	state.token = message
    },
    loggedOut (state) {
      localStorage.removeItem('token')
      localStorage.setItem('product_ids',new Array())
      localStorage.removeItem('user')
      state.token = null
      state.user = {
        name: '',
        email: '',
        mobile_number:'',
        is_mobile_verified:'',
        is_email_verified:''
      }
    },
    changeHeaderStatus(state, message) {
      state.showHeader = message
      state.showSideBar = message
    },
    removeFromCustomerArray(state, message){
      state.customer_array.splice(message, 1)
    },
    addToCustomerArray(state, message){
      state.customer_array.push(message)
    },
    removeAllCustomerArray(state) {
      state.customer_array = new Array()
    },
    addToWishlist(state, message) {
      state.product_ids.push(message)
      localStorage.setItem('product_ids', JSON.stringify(state.product_ids))
    },
    removeFromWishList(state, message) {
      var index = state.product_ids.indexOf(message);
      if (index > -1) {
        state.product_ids.splice(index, 1);
      }
      localStorage.setItem('product_ids', JSON.stringify(state.product_ids))
    },
    removeWishlist(state, message) {
      state.product_ids = new Array()
      localStorage.setItem('product_ids', JSON.stringify(state.product_ids))
    },
    userDetailsChange(state, message) {
      state.user = message
      localStorage.setItem('user', JSON.stringify(message))
    },
    increaseChunk(state, chunk) {
      state.pagination_chunk += chunk
    },
    resetChunk(state) {
      state.pagination_chunk = 0
    }
  }