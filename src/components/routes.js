module.exports = [
    {
        path: '/',
        component: resolve => require(['catalogue/CatalogueComponent.vue'], resolve), name: 'home',
        children: [
            {
                path: '/',
                component: resolve => require(['catalogue/BrandAndCategoriesView.vue'], resolve),
                name: 'catalogueView'
            },
            {
                path: 'brand/:id',
                component: resolve => require(['catalogue/SingleBrandComponent.vue'], resolve),
                name: 'singleBrand'
            },
            {
                path: 'category/:id',
                component: resolve => require(['catalogue/SingleCategoryComponent.vue'], resolve),
                name: 'singleCategory'
            }
        ]
    },
    // {
    //     path:'/product/:id', 
    //     component: resolve => require(['products/SingleProductComponent.vue'], resolve), name:'singleProduct'
    // },
    { path : '/login', component: resolve => require(['Auth/Login.vue'], resolve), name: 'login'},
    { path: '/signup',component: resolve => require(['Auth/SignUp.vue'], resolve), name: 'signup'},
    { path : '/logout', component: resolve => require(['Auth/Logout.vue'], resolve), name: 'logout'},
    { path: '/unauthorized', component: resolve => require(['common/Unauthorized.vue'], resolve), name:'unauthorized' },
    { path: '/no-roles', component: resolve => require(['common/NoRole.vue'], resolve), name:'norole' },
    { 
    	path : '/allusers', 
    	component: resolve => require(['contacts/ContactsComponent.vue'], resolve), name:'allusers',
	    	children: [
                    {
                        path:'/',
                        component: resolve => require(['contacts/AllContactsComponent.vue'], resolve), name: 'allContacts',
                    },
                    {
                        path:':id',
                        component: resolve => require(['wishlist/FriendWishlistComponent.vue'], resolve), name: 'FriendWishList',
                    }
	    	]
  	},
    {
        path: '/wishlist',
        component: resolve => require(['wishlist/WishListComponent.vue'], resolve),
            children:  [
                {
                    path: '/',
                    component: resolve => require(['wishlist/AllWishlistComponent.vue'], resolve), name: 'allWishlists',
                },
                {
                    path: 'sendwishlist',
                    component: resolve => require(['wishlist/SendWishlistComponent.vue'], resolve), name:'sendWishlist'
                },
                {
                    path: '/:id',
                    component: resolve => require(['wishlist/FriendWishlistComponent.vue'], resolve), name:'FriendWishlist'
                }

            ]
    }
  ]
