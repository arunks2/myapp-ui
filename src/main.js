import Vue from 'vue'
import App from './App.vue'
import VueResource from 'vue-resource'
import VueRouter from 'vue-router';
import VueFlatpickr from 'vue-flatpickr'
import VueParticles from 'vue-particles'
Vue.use(VueFlatpickr)
import Vuex from 'vuex';

Vue.use(Vuex)
Vue.use(VueRouter);
Vue.use(VueParticles)
Vue.use(VueResource);

const store = new Vuex.Store({
  state: require('./store/state.js'),
  mutations: require('./store/mutations.js')
})
const routes = require('./components/routes.js')

const router = new VueRouter({
	mode: 'history',
  	routes,
  	scrollBehavior (to, from, savedPosition) {
  	  if (savedPosition) {
  	    return savedPosition
  	  } else {
  	    return { x: 0, y: 0 }
  	  }
  	}
})
router.afterEach((to, from, next) => {
  	$('#Menu').sidebar('hide');
})

window.api_host = "http://myapp.test/api";
router.beforeEach((to, from, next) => {
    // console.log(to.name)
    if(to.name == null || to.name == 'login' || to.name == 'logout' || to.name == 'signup') {
      next();
    }
    else {
      var token = localStorage.getItem('token')
      $.ajax({ 
        type : "GET", 
        url : api_host+ '/test', 
        beforeSend: function(xhr){xhr.setRequestHeader('Authorization', token);},
        success : function(response) { 
          store.commit('changeHeaderStatus', true)
          next();
        }, 
        error : function(error) { 
          store.commit('changeHeaderStatus', false)
          next({'name':'login'});
        } 
    }); 
    }
    next()
})
new Vue({
  el: '#app',
  store,
  render: h => h(App), router
})
