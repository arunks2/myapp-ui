
> MyApp UI

## Build Setup

``` bash
# make sure you have node , npm install in your system.
# git clone this repository anywhere where node, npm are installed
# cd into the project directory 
# run npm run install to download all dependencies
# when all npm dependencies are completed


# (Don`t change this) Make sure to change api_host in main.js to whatever your Domain is. I had hosted it on http://myapp.test/api/

npm run dev

# serve with hot reload at localhost:8801, make sure this port is not used by any other service. If you want to change this to port of your availablity, look of file webpack.config.js, now under devServer change port to your liking
npm run dev


# Points to remember:
The App will not work in IOS incognito mode as IOS does not allow local storage there

```
For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
