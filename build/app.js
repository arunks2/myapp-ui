$(document).ready(function (event) {
	$('.ui.dropdown').dropdown();
	// $('.ui.checkbox').checkbox();
	$('.editModalButton').click(function(event) {
		event.preventDefault();
		$('#' + $(this).data('id')).modal('show');
	});
	var elems = $('.rolledUpOnPhone'), count = elems.length;
	$('.rolledUpOnPhone').each(function() {
		$(this).append('<div class="ui button basic blue RollerButton">' + $(this).data('title') + '</div>');
		if(!--count) {
			$('.RollerButton').on('click', function(event) {
				event.preventDefault();
				$(this).parent().children('.rollable').not('.RollerButton').slideToggle();
			})
		}
	});
})